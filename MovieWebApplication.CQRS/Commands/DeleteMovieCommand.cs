﻿using MediatR;

namespace MovieWebApplication.CQRS.Commands
{
    public record DeleteMovieCommand(Guid id) : IRequest<bool>;
}
