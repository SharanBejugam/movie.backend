﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Commands
{
    public record AddProducerCommand(Producer producer) : IRequest<bool>;
}
