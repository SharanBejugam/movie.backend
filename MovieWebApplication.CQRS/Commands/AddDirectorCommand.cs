﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Commands
{
    public record AddDirectorCommand(Director director) : IRequest<bool>;
}
