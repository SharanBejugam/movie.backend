﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Commands
{
    public record AddActorCommand(Actor actor) : IRequest<bool>;
}
