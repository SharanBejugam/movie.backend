﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Commands
{
    public record UpdateMovieCommand(Movie movie) : IRequest<bool>;
}
