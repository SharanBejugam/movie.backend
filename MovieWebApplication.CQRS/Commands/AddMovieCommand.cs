﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Commands
{
    public record AddMovieCommand(Movie movie) : IRequest<bool>;
}
