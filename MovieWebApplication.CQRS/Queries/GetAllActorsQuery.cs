﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Queries
{
    public record GetAllActorsQuery() : IRequest<IEnumerable<Actor>>;
}
