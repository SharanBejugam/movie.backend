﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Queries
{
    public record GetAllMoviesQuery : IRequest<IEnumerable<Movie>>;
}
