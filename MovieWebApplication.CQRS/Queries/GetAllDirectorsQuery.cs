﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Queries
{
    public record GetAllDirectorsQuery() : IRequest<IEnumerable<Director>>;
}
