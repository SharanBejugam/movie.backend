﻿using MediatR;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Queries
{
    public record GetMovieByIdQuery(Guid id) : IRequest<Movie>;
}
