﻿using MediatR;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.DataAccess.Interfaces;

namespace MovieWebApplication.CQRS.Handlers
{
    public class AddDirectorHandler : IRequestHandler<AddDirectorCommand, bool>
    {
        private readonly IDirectorRepository _repo;

        public AddDirectorHandler(IDirectorRepository repo)
        {
            _repo = repo;
        }

        public async Task<bool> Handle(AddDirectorCommand request, CancellationToken cancellationToken)
        {
            return await _repo.AddDirector(request.director);
        }
    }
}
