﻿using MediatR;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.DataAccess.Interfaces;

namespace MovieWebApplication.CQRS.Handlers
{
    public class AddProducerHandler : IRequestHandler<AddProducerCommand, bool>
    {
        private readonly IProducerRepository _repo;

        public AddProducerHandler(IProducerRepository repo)
        {
            _repo = repo;
        }

        public async Task<bool> Handle(AddProducerCommand request, CancellationToken cancellationToken)
        {
            return await _repo.AddProducer(request.producer);
        }
    }
}
