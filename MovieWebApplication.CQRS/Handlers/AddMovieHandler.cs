﻿using MediatR;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.DataAccess.Interfaces;

namespace MovieWebApplication.CQRS.Handlers
{
    public class AddMovieHandler : IRequestHandler<AddMovieCommand, bool>
    {
        private readonly IMovieRepository _repo;

        public AddMovieHandler(IMovieRepository repo)
        {
            _repo = repo;
        }

        public async Task<bool> Handle(AddMovieCommand request, CancellationToken cancellationToken)
        {
            return await _repo.AddMovie(request.movie);
        }
    }
}
