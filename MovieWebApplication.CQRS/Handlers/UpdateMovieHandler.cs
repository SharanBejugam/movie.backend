﻿using MediatR;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.DataAccess.Interfaces;

namespace MovieWebApplication.CQRS.Handlers
{
    public class UpdateMovieHandler : IRequestHandler<UpdateMovieCommand, bool>
    {
        private readonly IMovieRepository _repo;

        public UpdateMovieHandler(IMovieRepository repo)
        {
            _repo = repo;
        }

        public Task<bool> Handle(UpdateMovieCommand request, CancellationToken cancellationToken)
        {
            return _repo.UpdateMovie(request.movie);
        }
    }
}
