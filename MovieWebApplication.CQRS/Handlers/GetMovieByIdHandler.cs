﻿using MediatR;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Handlers
{
    public class GetMovieByIdHandler : IRequestHandler<GetMovieByIdQuery, Movie>
    {
        private readonly IMovieRepository _repo;

        public GetMovieByIdHandler(IMovieRepository repo)
        {
            _repo = repo;
        }

        public async Task<Movie> Handle(GetMovieByIdQuery request, CancellationToken cancellationToken)
        {
            return await _repo.GetMovieById(request.id);
        }
    }
}
