﻿using MediatR;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Handlers
{
    public class GetAllMoviesHandler : IRequestHandler<GetAllMoviesQuery, IEnumerable<Movie>>
    {
        private readonly IMovieRepository _repo;

        public GetAllMoviesHandler(IMovieRepository repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Movie>> Handle(GetAllMoviesQuery request, CancellationToken cancellationToken)
        {
            return await _repo.GetAllMovies();
        }
    }
}
