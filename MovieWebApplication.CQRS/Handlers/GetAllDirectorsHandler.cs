﻿using MediatR;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Handlers
{
    public class GetAllDirectorsHandler : IRequestHandler<GetAllDirectorsQuery, IEnumerable<Director>>
    {
        private readonly IDirectorRepository _repo;

        public GetAllDirectorsHandler(IDirectorRepository repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Director>> Handle(GetAllDirectorsQuery request, CancellationToken cancellationToken)
        {
            return await _repo.GetAllDirectors();
        }
    }
}
