﻿using MediatR;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.DataAccess.Interfaces;

namespace MovieWebApplication.CQRS.Handlers
{
    public class AddActorHandler : IRequestHandler<AddActorCommand, bool>
    {
        private readonly IActorRepository _repo;

        public AddActorHandler(IActorRepository repo)
        {
            _repo = repo;
        }

        public async Task<bool> Handle(AddActorCommand request, CancellationToken cancellationToken)
        {
            return await _repo.AddActor(request.actor);
        }
    }
}
