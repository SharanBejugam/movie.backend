﻿using MediatR;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.DataAccess.Interfaces;

namespace MovieWebApplication.CQRS.Handlers
{
    public class DeleteMovieHandler : IRequestHandler<DeleteMovieCommand, bool>
    {
        private readonly IMovieRepository _repo;

        public DeleteMovieHandler(IMovieRepository repo)
        {
            _repo = repo;
        }

        public Task<bool> Handle(DeleteMovieCommand request, CancellationToken cancellationToken)
        {
            return _repo.DeleteMovie(request.id);
        }
    }
}
