﻿using MediatR;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Handlers
{
    public class GetAllProducersHandler : IRequestHandler<GetAllProducersQuery, IEnumerable<Producer>>
    {
        private readonly IProducerRepository _repo;

        public GetAllProducersHandler(IProducerRepository repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Producer>> Handle(GetAllProducersQuery request, CancellationToken cancellationToken)
        {
            return await _repo.GetAllProducers();
        }
    }
}
