﻿using MediatR;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.CQRS.Handlers
{
    public class GetAllActorsHandler : IRequestHandler<GetAllActorsQuery, IEnumerable<Actor>>
    {
        private readonly IActorRepository _repo;

        public GetAllActorsHandler(IActorRepository repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Actor>> Handle(GetAllActorsQuery request, CancellationToken cancellationToken)
        {
            return await _repo.GetAllActors();
        }
    }
}
