using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;
using NSubstitute;
using Shouldly;

namespace MovieWebApplication.DataAccess.Test
{
    public class ActorRepositoryTest
    {
        List<Actor> _mockNullActors = new List<Actor>() { };
        List<Actor> _mockActors = new List<Actor>()
        {
                new Actor
                {
                    Id = new Guid("1d19c8c8-a33a-41c8-a56c-9dcc189ff6bc"),
                    FirstName = "TestName",
                    LastName = "TestName",
                    Age = 20,
                    Gender = "Male"
                },
                new Actor
                {
                    Id = new Guid("091a1356-5255-4912-b716-f04cef299e84"),
                    FirstName = "TestName1",
                    LastName = "TestName1",
                    Age = 21,
                    Gender = "Female"
                }
        };
        [Fact]
        public void ShouldReturnNull_WhenGetAllCalled()
        {

            //Arrange
            var actorRepository = Substitute.For<IActorRepository>();
            actorRepository.GetAllActors().Returns(_mockNullActors);
            //Act
            var sut = actorRepository.GetAllActors();
            //Assert
            sut.Result.ShouldBeEmpty();
            Assert.Empty(_mockNullActors);
        }

        [Fact]
        public async Task ShouldReturnListOfActors_WhenGetAllCalled()
        {
            //Arrange
            var test = new Actor()
            {
                Id = new Guid("1d19c8c8-a33a-41c8-a56c-9dcc189ff6bc"),
                FirstName = "TestName",
                LastName = "TestName",
                Age = 20,
                Gender = "Male"

            };
            var actorRepository = Substitute.For<IActorRepository>();
            actorRepository.GetAllActors().Returns(_mockActors);
            //Act
            var sut = await actorRepository.GetAllActors();
            //Assert
            sut.ShouldNotBeNull();
            Assert.NotEmpty(_mockActors);
            Assert.Equal<Actor>(_mockActors[0], test);
            Assert.True(_mockActors.Count == 2, "Expected 2 items but found {_mockActors.Count}");
            sut.ShouldBeOfType<List<Actor>>();
        }

        [Fact]
        public void ShouldretunValidActor_WhenGetByIdCalled()
        {
            //Arrange
            var actorRepository = Substitute.For<IActorRepository>();
            var actorid = new Guid("1d19c8c8-a33a-41c8-a56c-9dcc189ff6bc");
            actorRepository.GetActorById(actorid).Returns(_mockActors.FirstOrDefault(x => x.Id == actorid));
            //Act
            var sut = actorRepository.GetActorById(actorid);
            //Assert
            sut.ShouldNotBeNull();
            sut.Result.Id.ShouldBe(actorid);
            Assert.Contains<Actor>(_mockActors, item => item.Id == actorid); 
            sut.Result.ShouldBeOfType<Actor>();
        }
    }
}