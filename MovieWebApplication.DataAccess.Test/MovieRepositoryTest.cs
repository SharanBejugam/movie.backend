﻿using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;
using NSubstitute;
using Shouldly;

namespace MovieWebApplication.DataAccess.Test
{
    public class MovieRepositoryTest
    {
        List<Movie> _mockNullMovies = new List<Movie>() { };
        List<Movie> _mockMovies = new List<Movie>()
        {
            new Movie
            {
                Id = new Guid("edbbe4eb-a7a3-4550-ae4e-5e9e8bbe377b"),
                MovieName = "TestMovieName",
                ReleaseDate = "2000-10-12",
                Actors = "a1,a2",
                Directors = "d1,d2",
                Producers = "p1,p2",
                Rating = 3,
                TotalCost = 2000,
                Description = "good"
            },
            new Movie
            {
                Id = new Guid("14ad9b3c-f6bb-4c05-b34f-9697bfb95e15"),
                MovieName = "TestMovieName1",
                ReleaseDate = "2023-12-12",
                Actors = "a1,a2",
                Directors = "d1,d2",
                Producers = "p1,p2",
                Rating = 5,
                TotalCost = 200000,
                Description = "bad"
            }
        };
        IMovieRepository movieRepository = Substitute.For<IMovieRepository>();

        [Fact]
        public void ShouldReturnNull_WhenGetAllCalled()
        {

            //Arrange
            movieRepository.GetAllMovies().Returns(_mockNullMovies);
            //Act
            var sut = movieRepository.GetAllMovies();
            //Assert
            sut.Result.ShouldBeEmpty();
            Assert.Empty(_mockNullMovies);
        }

        [Fact]
        public void ShouldReturnListOfMovies_WhenGetAllCalled()
        {
            //Arrange
            var test = new Movie()
            {
                Id = new Guid("edbbe4eb-a7a3-4550-ae4e-5e9e8bbe377b"),
                MovieName = "TestMovieName",
                ReleaseDate = "2000-10-12",
                Actors = "a1,a2",
                Directors = "d1,d2",
                Producers = "p1,p2",
                Rating = 3,
                TotalCost = 2000,
                Description = "good"
            };
            movieRepository.GetAllMovies().Returns(_mockMovies);
            //Act
            var sut = movieRepository.GetAllMovies();
            //Assert
            sut.ShouldNotBeNull();
            Assert.NotEmpty(_mockMovies);
            Assert.Equal<Movie>(_mockMovies[0], test);
            Assert.True(_mockMovies.Count == 2, $"Expected 2 items but found {_mockMovies.Count}");
            sut.Result.ShouldBeOfType<List<Movie>>();
            Assert.IsType<List<Movie>>(sut.Result);
        }

        [Fact]
        public void ShouldretunValidMovie_WhenGetByIdCalled()
        {
            //Arrange
            var movieid = new Guid("edbbe4eb-a7a3-4550-ae4e-5e9e8bbe377b");
            movieRepository.GetMovieById(movieid).Returns(_mockMovies.FirstOrDefault(x => x.Id == movieid));
            //Act
            var sut = movieRepository.GetMovieById(movieid);
            //Assert
            sut.ShouldNotBeNull();
            Assert.NotEmpty(_mockMovies);
            sut.Result.Id.ShouldBe(movieid);
            Assert.Contains<Movie>(_mockMovies, item => item.Id == movieid);
            sut.Result.ShouldBeOfType<Movie>();
            Assert.IsType<Movie>(sut.Result);
        }

        [Fact]
        public void ShouldAddMovie_WhenAddMovieCalled()
        {
            //Arrange
            var movieid = new Guid();
            var newMovie = new Movie()
            {
                Id = movieid,
                MovieName = "TestName2",
                ReleaseDate = "2001-04-12",
                Actors = "a1,a2",
                Directors = "d1,d2",
                Producers = "p1,p2",
                Rating = 5,
                TotalCost = 12000,
                Description = "Good"
            };
            movieRepository.AddMovie(newMovie).Returns(true);
            //Act
            var sut = movieRepository.AddMovie(newMovie);
            //Assert
            sut.ShouldNotBeNull();
            Assert.NotNull(sut);
            sut.Result.ShouldBeTrue();
            Assert.True(sut.Result);
            Assert.Contains<Movie>(_mockMovies, item => item.Id == movieid);
            Assert.True(_mockMovies.Count == 3, $"Expected 3 items but found {_mockMovies.Count}");
        }

        [Fact]
        public void ShouldDeleteMovie_WhenDeleteMovieCalled()
        {
            //Arrange
            var movieid = new Guid("edbbe4eb-a7a3-4550-ae4e-5e9e8bbe377b");
            movieRepository.DeleteMovie(movieid).Returns(true);
            //Act
            var sut = movieRepository.DeleteMovie(movieid);
            //Assert
            sut.Result.ShouldBeTrue();
            Assert.True(sut.Result);
            Assert.DoesNotContain<Movie>(_mockMovies, item => item.Id == movieid);
            Assert.True(_mockMovies.Count == 2, $"Expected 2 items but found {_mockMovies.Count}");
        }

        [Fact]
        public void ShouldUpdateMovie_WhenUpdateMovieCalled()
        {
            //Arrange
            var updateMovie = new Movie()
            {
                Id = new Guid("edbbe4eb-a7a3-4550-ae4e-5e9e8bbe377b"),
                MovieName = "Updated Movie",
                ReleaseDate = "2000-10-12",
                Actors = "a1,a2",
                Directors = "d1,d2",
                Producers = "p1,p2",
                Rating = 3,
                TotalCost = 2000,
                Description = "Bad"
            };
            movieRepository.UpdateMovie(updateMovie).Returns(true);
            //Act
            var sut = movieRepository.UpdateMovie(updateMovie);
            //Assert
            sut.ShouldNotBeNull();
            Assert.NotNull(sut);
            Assert.True(sut.Result);
            sut.Result.ShouldBeTrue();
            Assert.Equal<Movie>(updateMovie, _mockMovies.FirstOrDefault(x => x.Id == updateMovie.Id));
        }
    }
}
