﻿using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.DataAccess.Repositories;
using MovieWebApplication.Domains.Models;
using NSubstitute;
using Shouldly;

namespace MovieWebApplication.DataAccess.Test
{
    public class ProducerRepositoryTest
    {
        List<Producer> _mockNullProducers = new List<Producer>() { };
        List<Producer> _mockProducers = new List<Producer>()
        {
            new Producer
            {
                Id = new Guid("44a5f441-cbcc-4bf4-94e7-54f051f93850"),
                FirstName = "TestName",
                LastName = "TestName",
                Age = 30,
                Gender = "Male"
            },
            new Producer
            {
                Id = new Guid("edbbe4eb-a7a3-4550-ae4e-5e9e8bbe377b"),
                FirstName = "TestName1",
                LastName = "TestName1",
                Age = 31,
                Gender = "Female"
            }
        };

        [Fact]
        public void ShouldReturnNull_WhenGetAllCalled()
        {

            //Arrange
            var producerRepository = Substitute.For<IProducerRepository>();
            producerRepository.GetAllProducers().Returns(_mockNullProducers);
            //Act
            var sut = producerRepository.GetAllProducers();
            //Assert
            sut.Result.ShouldBeEmpty();
            Assert.Empty(_mockNullProducers);
        }

        [Fact]
        public void ShouldReturnListOfProducers_WhenGetAllCalled()
        {

            //Arrange
            var test = new Producer()
            {
                Id = new Guid("44a5f441-cbcc-4bf4-94e7-54f051f93850"),
                FirstName = "TestName",
                LastName = "TestName",
                Age = 30,
                Gender = "Male"
            };
            var producerRepository = Substitute.For<IProducerRepository>();
            producerRepository.GetAllProducers().Returns(_mockProducers);
            //Act
            var sut = producerRepository.GetAllProducers();
            //Assert
            sut.ShouldNotBeNull();
            Assert.NotEmpty(_mockProducers);
            Assert.Equal<Producer>(_mockProducers[0], test);
            Assert.True(_mockProducers.Count == 2, "Expected 2 items but found {_mockProducers.Count}");
            sut.Result.ShouldBeOfType<List<Producer>>();
        }
    }
}
