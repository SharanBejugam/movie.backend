﻿using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;
using NSubstitute;
using Shouldly;

namespace MovieWebApplication.DataAccess.Test
{
    public class DirectorRepositoryTest
    {
        List<Director> _mockNullDirectors = new List<Director>() { };
        List<Director> _mockDirectors = new List<Director>()
        {
            new Director
            {
                Id = new Guid("44a5f441-cbcc-4bf4-94e7-54f051f93850"),
                FirstName = "TestName",
                LastName = "TestName",
                Age = 30,
                Gender = "Male"
            },
            new Director
            {
                Id = new Guid( "14ad9b3c-f6bb-4c05-b34f-9697bfb95e15"),
                FirstName = "TestName1",
                LastName = "TestName1",
                Age = 31,
                Gender = "Female"
            }
        };
        IDirectorRepository directorRepository = Substitute.For<IDirectorRepository>();

        [Fact]
        public void ShouldReturnNull_WhenGetAllCalled()
        {

            //Arrange
            directorRepository.GetAllDirectors().Returns(_mockNullDirectors);
            //Act
            var sut = directorRepository.GetAllDirectors();
            //Assert
            sut.Result.ShouldBeEmpty();
            Assert.Empty(_mockNullDirectors);
        }

        [Fact]
        public void ShouldReturnListOfDirectors_WhenGetAllCalled()
        {

            //Arrange
            var test = new Director()
            {
                Id = new Guid("44a5f441-cbcc-4bf4-94e7-54f051f93850"),
                FirstName = "TestName",
                LastName = "TestName",
                Age = 30,
                Gender = "Male"
            };
            directorRepository.GetAllDirectors().Returns(_mockDirectors);
            //Act
            var sut = directorRepository.GetAllDirectors();
            //Assert
            sut.ShouldNotBeNull();
            Assert.NotEmpty(_mockDirectors);
            Assert.Equal<Director>(_mockDirectors[0], test);
            Assert.True(_mockDirectors.Count == 2, "Expected 2 items but found {_mockDirectors.Count}");
            sut.Result.ShouldBeOfType<List<Director>>();
        }
    }
}
