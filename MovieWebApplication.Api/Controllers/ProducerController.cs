﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProducerController : ControllerBase
    {
        private readonly IMediator _sender;

        public ProducerController(IMediator sender)
        {
            _sender = sender;
        }

        [HttpGet("getallproducers")]
        public async Task<IEnumerable<Producer>> GetAllProducers()
        {
            return await _sender.Send(new GetAllProducersQuery());
        }

        [HttpPost("addproducer")]
        public async Task<bool> AddProducer(Producer producer)
        {
            return await _sender.Send(new AddProducerCommand(producer));
        }
    }
}
