﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.Domains.Models;
using System.ComponentModel.DataAnnotations;

namespace MovieWebApplication.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly IMediator _sender;

        public MovieController(IMediator sender)
        {
            _sender = sender;
        }

        [HttpGet("getallmovies")]
        public async Task<IEnumerable<Movie>> GetAllMovies()
        {
            return await _sender.Send(new GetAllMoviesQuery());
        }

        [HttpGet("getmoviebyid")]
        public async Task<Movie> GetMovieById([Required] Guid id)
        {
            return await _sender.Send(new GetMovieByIdQuery(id));
        }

        [HttpPost("addmovie")]
        public async Task<bool> AddMovie(Movie movie)
        {
            return await _sender.Send(new AddMovieCommand(movie));
        }

        [HttpDelete("deletemovie")]
        public async Task<bool> DeleteMovie(Guid id)
        {
            return await _sender.Send(new DeleteMovieCommand(id));
        }

        [HttpPut("updatemovie")]
        public async Task<bool> UpdateMovie(Movie movie)
        {
            return await _sender.Send(new UpdateMovieCommand(movie));
        }
    }
}
