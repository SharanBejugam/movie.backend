﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectorController : ControllerBase
    {
        private readonly IMediator _sender;

        public DirectorController(IMediator sender)
        {
            _sender = sender;
        }

        [HttpGet("getalldirectors")]
        public async Task<IEnumerable<Director>> GetAllDirectors()
        {
            return await _sender.Send(new GetAllDirectorsQuery());
        }

        [HttpPost("adddirector")]
        public async Task<bool> AddProducer(Director director)
        {
            return await _sender.Send(new AddDirectorCommand(director));
        }
    }
}
