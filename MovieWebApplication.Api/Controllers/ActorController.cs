﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MovieWebApplication.CQRS.Commands;
using MovieWebApplication.CQRS.Queries;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly IMediator _sender;

        public ActorController(IMediator sender)
        {
            _sender = sender;
        }

        [HttpGet]
        public async Task<IEnumerable<Actor>> GetAllActors()
        {
            return await _sender.Send(new GetAllActorsQuery());
        }

        [HttpPost]
        public async Task<bool> Add(Actor actor)
        {
            return await _sender.Send(new AddActorCommand(actor));
        }
    }
}
