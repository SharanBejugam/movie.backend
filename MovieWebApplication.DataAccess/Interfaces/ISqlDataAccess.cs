﻿namespace MovieWebApplication.DataAccess.Interfaces
{
    public interface ISqlDataAccess
    {
        Task<IEnumerable<T>> LoadData<T>(string sqlQuery, object parameters);
        Task SaveData(string sqlquery, object parameters);
    }
}