﻿using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Interfaces
{
    public interface IProducerRepository
    {
        Task<bool> AddProducer(Producer producer);
        Task<IEnumerable<Producer>> GetAllProducers();
    }
}