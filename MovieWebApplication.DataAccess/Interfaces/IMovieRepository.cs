﻿using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Interfaces
{
    public interface IMovieRepository
    {
        Task<bool> AddMovie(Movie movie);
        Task<IEnumerable<Movie>> GetAllMovies();
        Task<Movie> GetMovieById(Guid id);
        Task<bool> DeleteMovie(Guid id);
        Task<bool> UpdateMovie(Movie movie);
    }
}