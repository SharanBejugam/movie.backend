﻿using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Interfaces
{
    public interface IDirectorRepository
    {
        Task<bool> AddDirector(Director director);
        Task<IEnumerable<Director>> GetAllDirectors();
    }
}