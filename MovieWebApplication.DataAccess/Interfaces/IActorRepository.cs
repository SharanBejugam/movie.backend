﻿using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Interfaces
{
    public interface IActorRepository
    {
        Task<bool> AddActor(Actor actor);
        Task<Actor> GetActorById(Guid id);
        Task<IEnumerable<Actor>> GetAllActors();
    }
}