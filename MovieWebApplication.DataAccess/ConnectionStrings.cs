﻿namespace MovieWebApplication.DataAccess
{
    public class ConnectionStrings
    {
        public string? DataCon { get; set; }
        public string? DataSqlite { get; set; }
    }
}
