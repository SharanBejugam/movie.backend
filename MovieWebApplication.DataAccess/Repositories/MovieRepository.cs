﻿using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly ISqlDataAccess _db;

        public MovieRepository(ISqlDataAccess db) => _db = db;

        public async Task<IEnumerable<Movie>> GetAllMovies()
        {
            var data = await _db.LoadData<Movie>("SELECT [Id], [MovieName], [ReleaseDate], [Actors], [Directors], [Producers], [Rating], [TotalCost], [Description] FROM [dbo].[MoviesTable]", new { });
            return data;
        }

        public async Task<Movie> GetMovieById(Guid id)
        {
            var query = "SELECT * FROM dbo.MoviesTable WHERE Id = @id";
            var result = await _db.LoadData<Movie>(query, new { id });
            return result.FirstOrDefault();
        }

        public async Task<bool> AddMovie(Movie movie)
        {
            var guid = Guid.NewGuid();
            var query = "INSERT INTO [dbo].[MoviesTable] ([Id], [MovieName], [ReleaseDate], [Actors], [Directors], [Producers], [Rating], [TotalCost],[Description]) VALUES(@Id,@MovieName,@ReleaseDate,@actors,@directors,@producers,@rating,@totalcost,@description)";
            await _db.SaveData(query, new
            {
                Id = guid,
                movie.MovieName,
                movie.ReleaseDate,
                movie.Actors,
                movie.Directors,
                movie.Producers,
                movie.Rating,
                movie.TotalCost,
                movie.Description
            });
            return true;
        }

        public async Task<bool> DeleteMovie(Guid id)
        {
            await _db.SaveData("DELETE FROM [dbo].[MoviesTable] WHERE Id = @id", new { id });
            return true;
        }

        public async Task<bool> UpdateMovie(Movie movie)
        {
            var query = "UPDATE [dbo].[MoviesTable] SET [MovieName] = @movieName, [ReleaseDate] = @relaseDate, [Actors] = @actors, [Directors] = @directors, [Producers] = @producers, [Rating] = @rating, [TotalCost] = @totalCost, [Description] = @description WHERE [Id] = @id";

            await _db.SaveData(query, new
            {
                movie.MovieName,
                movie.ReleaseDate,
                movie.Actors,
                movie.Directors,
                movie.Producers,
                movie.Rating,
                movie.TotalCost,
                movie.Description,
                movie.Id
            });
            return true;
        }
    }
}
