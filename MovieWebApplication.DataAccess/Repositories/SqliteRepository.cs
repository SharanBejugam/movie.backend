﻿using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Options;
using MovieWebApplication.DataAccess.Interfaces;
using System.Data;

namespace MovieWebApplication.DataAccess.Repositories
{
    public class SqliteRepository : ISqlDataAccess
    {
        private IOptions<ConnectionStrings> _config;

        public SqliteRepository(IOptions<ConnectionStrings> config) => _config = config;

        public async Task<IEnumerable<T>> LoadData<T>(string sqlQuery, object parameters)
        {
            using var connection = new SqliteConnection(_config.Value.DataSqlite);
            return await connection.QueryAsync<T>(sqlQuery, parameters, commandType: CommandType.Text);
        }

        public async Task SaveData(string sqlquery, object parameters)
        {
            using var connection = new SqliteConnection(_config.Value.DataSqlite);
            await connection.ExecuteAsync(sqlquery, parameters, commandType: CommandType.Text);
        }
    }
}
