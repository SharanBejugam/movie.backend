﻿using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Repositories
{
    public class ProducerRepository : IProducerRepository
    {
        private readonly ISqlDataAccess _db;

        public ProducerRepository(ISqlDataAccess db) => _db = db;

        public async Task<IEnumerable<Producer>> GetAllProducers() =>
            await _db.LoadData<Producer>("SELECT * FROM [dbo].[ProducersTable]", new { });

        public async Task<bool> AddProducer(Producer producer)
        {
            var guid = Guid.NewGuid();
            await _db.SaveData("INSERT INTO [dbo].[ProducersTable] ([Id], [FirstName], [LastName], [Age], [Gender]) VALUES (@id, @firstName, @lastName, @age, @gender)", new
            {
                Id = guid,
                producer.FirstName,
                producer.LastName,
                producer.Age,
                producer.Gender
            });
            return true;
        }
    }
}
