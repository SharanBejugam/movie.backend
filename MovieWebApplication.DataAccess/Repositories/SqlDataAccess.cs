﻿using Dapper;
using Microsoft.Extensions.Options;
using MovieWebApplication.DataAccess.Interfaces;
using System.Data;
using System.Data.SqlClient;

namespace MovieWebApplication.DataAccess.Repositories
{
    public class SqlDataAccess : ISqlDataAccess
    {
        private IOptions<ConnectionStrings> _config;

        public SqlDataAccess(IOptions<ConnectionStrings> config) => _config = config;

        public async Task<IEnumerable<T>> LoadData<T>(string sqlQuery, object parameters)
        {
            using IDbConnection connection = new SqlConnection(_config.Value.DataCon);
            return await connection.QueryAsync<T>(sqlQuery, parameters, commandType: CommandType.Text);
        }

        public async Task SaveData(string sqlquery, object parameters)
        {
            using IDbConnection connection = new SqlConnection(_config.Value.DataCon);
            await connection.ExecuteAsync(sqlquery, parameters, commandType: CommandType.Text);
        }
    }
}
