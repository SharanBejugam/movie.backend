﻿using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Repositories
{
    public class ActorRepository : IActorRepository
    {
        private readonly ISqlDataAccess _db;

        public ActorRepository(ISqlDataAccess db) => _db = db;

        public async Task<IEnumerable<Actor>> GetAllActors() =>
            await _db.LoadData<Actor>("SELECT * FROM ActorsTable", new { });

        public async Task<Actor> GetActorById(Guid id)
        {
            var result = await _db.LoadData<Actor>("SELECT * FROM dbo.ActorsTable WHERE Id = @id", new { id });
            return result.FirstOrDefault();
        }

        public async Task<bool> AddActor(Actor actor)
        {
            var guid = Guid.NewGuid();
            await _db.SaveData("INSERT INTO ActorsTable (Id, FirstName, LastName, Age, Gender) VALUES (@id, @firstName, @lastName, @age, @gender)", new
            {
                Id = guid,
                actor.FirstName,
                actor.LastName,
                actor.Age,
                actor.Gender
            });
            return true;
        }
    }
}
