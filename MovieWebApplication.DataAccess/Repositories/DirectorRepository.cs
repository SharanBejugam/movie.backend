﻿using MovieWebApplication.DataAccess.Interfaces;
using MovieWebApplication.Domains.Models;

namespace MovieWebApplication.DataAccess.Repositories
{
    public class DirectorRepository : IDirectorRepository
    {
        private readonly ISqlDataAccess _db;

        public DirectorRepository(ISqlDataAccess db) => _db = db;

        public async Task<IEnumerable<Director>> GetAllDirectors() =>
            await _db.LoadData<Director>("SELECT * FROM [dbo].[DirectorsTable]", new { });

        public async Task<bool> AddDirector(Director director)
        {
            var guid = Guid.NewGuid();
            await _db.SaveData("INSERT INTO [dbo].[DirectorsTable] ([Id], [FirstName], [LastName], [Age], [Gender]) VALUES (@id, @firstName, @lastName, @age, @gender)", new
            {
                Id = guid,
                director.FirstName,
                director.LastName,
                director.Age,
                director.Gender
            });
            return true;
        }
    }
}
