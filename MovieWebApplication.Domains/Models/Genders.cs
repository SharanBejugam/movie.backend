﻿namespace MovieWebApplication.Domains.Models
{
    public enum Genders : int
    {
        Male = 1,
        Female = 2,
        Other = 3,
    }
}
