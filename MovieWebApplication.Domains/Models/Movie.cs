﻿namespace MovieWebApplication.Domains.Models
{
    public class Movie
    {
        public Guid Id { get; set; }
        public string? MovieName { get; set; }
        public string? ReleaseDate { get; set; }
        public string? Actors { get; set; }
        public string? Directors { get; set; }
        public string? Producers { get; set; }
        public int Rating { get; set; }
        public long TotalCost { get; set; }
        public string? Description { get; set; }

    }
}
